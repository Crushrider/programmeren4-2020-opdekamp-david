<?php
include 'convertor_function.php';

$from ='';
$fromUnit = 'm';
$to = '';
$toUnit = 'cm';

if($_SERVER["REQUEST_METHOD"]=="POST")
    $from = htmlspecialchars($_POST['from']);
    $fromUnit = htmlspecialchars($_POST['from_unit']);
    $toUnit=htmlspecialchars( $_POST['from unit']);

    if( is_numeric($from) or empty($from)){
        $hasError==true;
        $errorMessage = 'eerste veld is leeg of bevat geen getal';
    }
    else{
        $to = convert($from,$fromUnit,$toUnit);
    }
    $to = convert($from,$fromUnit,$toUnit);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Convertor</title>
</head>

<body>

<h1>Convertor van eenheden</h1>
<?php if($hasError) : ?>
    <p style ="color:red" ></p>
<?php endif; ?>
<form method="post" action="#">
    <input type="text" name="from" id="form" value="<?=$from?>">
    <select name="from_unit" id="from_unit">
        <option value="m" <?php if($fromUnit=='m') echo 'selected'?>>Meter</option>
        <option value="cm" <?php if($fromUnit=='cm') echo 'selected'?>>Centimeter</option>
        <option value="km" <?php if($fromUnit=='km') echo 'selected'?>>Kilometer</option>
        <option value="in" <?php if($fromUnit=='in') echo 'selected'?>>Inch</option>
        <option value="ft" <?php if($fromUnit=='ft') echo 'selected'?>>Foot</option>
        <option value="yd" <?php if($fromUnit=='yd') echo 'selected'?>>Yard</option>
        <option value="mi" <?php if($fromUnit=='mi') echo 'selected'?>>Mile</option>
        <option value="au" <?php if($fromUnit=='au') echo 'selected'?>>Astronomical Unit</option>
        <option value="ly" <?php if($fromUnit=='ly') echo 'selected'?>>Light Year </option>
        <option value="pc" <?php if($fromUnit=='pc') echo 'selected'?>>Parsec</option>
    </select>
    <input type="text" name="to" id="to" value="<?=$to?>">
    <select name="to_unit" id="to_unit">
        <option value="m" <?php if($toUnit=='m') echo 'selected'?>>Meter</option>
        <option value="cm" <?php if($toUnit=='cm') echo 'selected'?>>Centimeter</option>
        <option value="km" <?php if($toUnit=='km') echo 'selected'?>>Kilometer</option>
        <option value="in" <?php if($toUnit=='in') echo 'selected'?>>Inch</option>
        <option value="ft" <?php if($toUnit=='ft') echo 'selected'?>>Foot</option>
        <option value="yd" <?php if($toUnit=='yd') echo 'selected'?>>Yard</option>
        <option value="mi" <?php if($toUnit=='mi') echo 'selected'?>>Mile</option>
        <option value="au" <?php if($toUnit=='au') echo 'selected'?>>Astronomical Unit</option>
        <option value="ly" <?php if($toUnit=='ly') echo 'selected'?>>Light Year </option>
        <option value="pc" <?php if($toUnit=='pc') echo 'selected'?>>Parsec</option>
    </select>
</form>

</body>

</html>
